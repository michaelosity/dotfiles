colorscheme twilight256         " colorscheme
inoremap jj <ESC>
set tabstop=2                   " number of visual spaces per tab
set softtabstop=2               " number of spaces in a tab when editing
set expandtab                   " tabs --> spaces
set number 		                  " show line numbers
set cursorline                  " highlight current line
set encoding=utf-8              " utf-8
set lazyredraw                  " only redraw when necessary
set showcmd                     " show last command in lower right corner
set showmatch                   " show matching parenthesis
set foldenable                  " enable folding
set foldlevelstart=10           " open most folds by default
set foldmethod=indent           " fold based on indent level
let mapleader=","
syntax enable                   " color-code files
filetype plugin indent on


